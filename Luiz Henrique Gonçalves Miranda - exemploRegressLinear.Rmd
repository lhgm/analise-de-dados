---
output:
  html_document: default
  word_document: default
  pdf_document: default
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```



# Exemplo de regressao simples
Le dados colesterol e faz uma regressao simples tendo como co-variavel a idade

```{r read}
ex<-read.table("colesterol.txt",header=T)
ex
fit<-lm(colesterol~idade,data=ex)
summary(fit)
anova(fit)
attach(ex)
attach(fit)
# R-Quadrado ajustado a uma medida semelhante ao R-quadrado mas que, ao contrario deste, nao aumenta com a inclusao de variaveis independentes nao significativas. O R-quadrado tende a  aumentar sempre que sao adicionadas novas variaveis independentes, mesmo que contribuam pouco para o poder explicativo da regressao.
```
E fazemos analise de residuos

```{r}
plot(idade,residuals)
title("Linearidade")
#  Na Regressao Linear Simples, tem o mesmo papel do grafico residuos vs valores estimados.
# Em Regressao Multipla, pode ser usado para verificar a necessidade de se incluir variaveis. E feito o grafico dos residuos vs variaveis nao incluidas no modelo. Se houver algum padrao, significa que a variavel deve ser incluida.

qqnorm(residuals)
qqline(residuals)
plot(fitted.values,residuals)
title("Homogeneidade da Variancia")
# se os pontos estao aleatoriamente distribuidos em torno do 0, sem nenhum comportamento ou tendencia, temos indicios de que a variancia dos residuos e a mesma para as observacoes.

plot(1:24,residuals,xlab='indice')
title("Independencia")
# se os pontos tiverem um comportamento que se repete em determinado ponto do grafico, temos indicios de dependencia dos residuos.A presenca de algum padrro indica correlacao entre os residuos.
```

Predicao incluindo intervalo confianca
```{r}
plot(idade, colesterol, pch = 18)
abline(fit, col = "red")
newX = seq(min(idade), max(idade), 1)
prd.CI = predict(fit, newdata = data.frame(idade=newX), interval = "confidence", level = 0.95) #intervalo de confianca: leva em consideracao a incerteza da parametro estimado
# o intervalo contera o verdadeiro par?metro com 95% de probabilidade 
lines(newX, prd.CI[, 2], col = "blue", lty = 2)
lines(newX, prd.CI[, 3], col = "blue", lty = 2)
prd.PI = predict(fit, newdata = data.frame(idade=newX), interval = "prediction", level = 0.95)
#intervalo de predicao: leva em consideracao a incerteza do parametro estimado e da predicao
# Um intervalo de predi??o ? um intervalo associado a uma vari?vel aleat?ria ainda a ser observada.
# O verdadeiro valor da predicao deve estar dentro do intervalo com probabilidade de 0.95.
lines(newX, prd.PI[, 2], col = "green", lty = 3)
lines(newX, prd.PI[, 3], col = "green", lty = 3)
```

